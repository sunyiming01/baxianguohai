# 理发预约系统

#### 介绍
八仙过海项目团队 理发预约系统
后台完成度：70%
前台完成度：20%

#### 软件架构
软件架构说明
本项目后端采用springboot开发
前端采用vue开发
包括前台（客户端）和后台（管理员端）
前台包括首页、预约下单、商铺信息、优惠活动、用户分享、美发咨询、留言分享、个人中心共8个功能模块
后台包括首页、个人中心、用户管理、商铺信息管理、美发信息管理、优惠活动管理、用户预约管理、用户分享管理、管理员管理、留言板管理、系统管理共10个功能模块
#### 安装教程
在/src/main/resources/config.properties中编辑：
											
	jdbc_url=jdbc:mysql://127.0.0.1:3306/ssmun1dm?useUnicode=true&characterEncoding=UTF-8&tinyInt1isBit=false
	jdbc_username=root	    数据库用户名 root
	jdbc_password=123456	用户密码    123456

#### 使用说明
后台地址：http://localhost:8888/ssmun1dm/admin/dist/index.html
管理员 abo  密码：abo  
前台地址：http://localhost:8888/ssmun1dm/front/index.html
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
